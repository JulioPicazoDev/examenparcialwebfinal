package com.softtek.academy.javaweb.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivitiesService;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

/**
 * ControllerServlet
 */
public class UpdActs extends HttpServlet {
	
	ActivitiesService actServ = new ActivitiesService();
	
    public UpdActs() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Boolean _getAct = actServ.UpdateList(Integer.parseInt(request.getParameter("id")),Integer.parseInt(request.getParameter("done")));
        if(_getAct)
        {
            List<ActivityBean> acts = actServ.getAllList(0 ,true);
            
            RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            request.setAttribute("list", acts);
            rd.forward(request, response);
        }
        else 
        {
            RequestDispatcher rd = request.getRequestDispatcher("/views/error.jsp");
            rd.forward(request, response);
        }
    }

}