package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivitiesService;

public class AddAct extends HttpServlet {
	
	 ActivitiesService actServ = new ActivitiesService();
     ActivitiesService actServ2 = new ActivitiesService();    
	
    public AddAct() {
        super();

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String act = request.getParameter("list");
        Boolean DONE = Boolean.valueOf(request.getParameter("done"));
        int validate = actServ.createTodo(act, DONE  == false ? 0 : 1);

        if(validate == 0)
        {
            List<ActivityBean> acts = actServ2.getAllList(0 ,true);
            
            RequestDispatcher rd = request.getRequestDispatcher("/views/listToDo.jsp");
            request.setAttribute("list", acts);
            rd.forward(request, response);
        } 
        else 
        {
            RequestDispatcher rd = request.getRequestDispatcher("");
            rd.forward(request, response);
        }
    }

}
