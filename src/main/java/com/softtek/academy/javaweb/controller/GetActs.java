package com.softtek.academy.javaweb.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.ActivityBean;
import com.softtek.academy.javaweb.service.ActivitiesService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

/**
 * ControllerServlet
 */
public class GetActs extends HttpServlet {

	List<ActivityBean> acts = new ArrayList<ActivityBean>();
    ActivitiesService actServ = new ActivitiesService();
    
    public GetActs() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String DONE = request.getParameter("done");
        
        if (DONE != null && !DONE.isEmpty()) 
        { 
        	acts = actServ.getAllList(Integer.parseInt(DONE), false);
        }
        else 
        {
        	acts = actServ.getAllList(0, true);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/views/listToDo.jsp");
        request.setAttribute("list", acts);
        rd.forward(request, response);
    }

}