
package com.softtek.academy.javaweb.service;

import java.util.List;

import com.softtek.academy.javaweb.dao.AddActDAO;
import com.softtek.academy.javaweb.dao.GetActDAO;
import com.softtek.academy.javaweb.dao.UpdActDAO;
import com.softtek.academy.javaweb.model.ActivityBean;

/**
 * ToDoList.service
 */
public class ActivitiesService {
    //Instances
	GetActDAO getActivitiesDAO = new GetActDAO();
	UpdActDAO updActivitiesDAO = new UpdActDAO();
	AddActDAO addActivitiesDAO = new AddActDAO();
	
    public List<ActivityBean> getAllList(int _IsDone, Boolean _acts){      
        return getActivitiesDAO.getActs(_IsDone, _acts);
    }

    public Boolean UpdateList(int _idAct, int _IsDone){      
        return updActivitiesDAO.updActs(_idAct, _IsDone);
    }
    
    public int createTodo(String _acts, int _IsDone){ 	
        return addActivitiesDAO.addAct(_acts,_IsDone);
    }

    
}