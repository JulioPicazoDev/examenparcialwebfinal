package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Connections;
import com.softtek.academy.javaweb.model.ConexDB;
import com.softtek.academy.javaweb.model.ActivityBean;

public class GetActDAO {
	
	List<ActivityBean> act = new ArrayList<ActivityBean>();
	
    public List<ActivityBean> getActs(int _Isdone, Boolean _acts) {
       
        try  {
        	Connection conex = ConexDB.getInstance().getConnection();
        	ResultSet rs = null;
            if (_acts) 
            {
                rs = conex.prepareStatement("select * from TO_DO_LIST").executeQuery();
            } 
            else 
            {
                PreparedStatement ps = conex.prepareStatement("select * from TO_DO_LIST where is_done = ?");
                ps.setInt(1, _Isdone);
                rs = ps.executeQuery();
            }
            while (rs.next()) 
            {
            	act.add(new ActivityBean(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
            return act;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
