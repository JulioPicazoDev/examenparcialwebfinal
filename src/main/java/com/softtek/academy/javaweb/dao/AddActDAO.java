package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.softtek.academy.javaweb.model.ConexDB;

public class AddActDAO {
	
	   public int addAct(String _actName, int _Isdone) {
	        try  {
	        	Connection conex = ConexDB.getInstance().getConnection();
	        	
	        	PreparedStatement ps = conex.prepareStatement("insert into TO_DO_LIST (list, is_done) values (?, ?)",Statement.RETURN_GENERATED_KEYS);
	            ps.setString(1, _actName);
	            ps.setInt(2, _Isdone);
	            ps.executeUpdate();
	            
	            ResultSet rs = ps.getGeneratedKeys();
	            if (rs.next()) 
	            {
	                return  rs.getInt(1);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            return 0;
	        }
	        return 0;
	    }

}
