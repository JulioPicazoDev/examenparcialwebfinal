package com.softtek.academy.javaweb.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DBConection
 */
public class ConexDB {
	private static ConexDB conex;
	private Connection connection;
	private String uri = "jdbc:mysql://localhost:3306/prueba?serverTimezone=UTC#";
	private String username = "root";
	private String password = "1234";

	private ConexDB() throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.connection = DriverManager.getConnection(uri, username, password);
		} catch (ClassNotFoundException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static ConexDB getInstance() throws SQLException {
		if (conex == null) {
			conex = new ConexDB();
		} else {
			return conex;
		}
		return conex;

	}
}