package com.softtek.academy.javaweb.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class Connections {

	private static Connections conex;
	private Connection connection;
	private String uri = "jdbc:mysql://localhost:3306/prueba?serverTimezone=UTC#";
	private String username = "root";
	private String password = "1234";

	private Connections() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(uri, username, password);
		} catch (ClassNotFoundException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static Connections getInstance() throws SQLException {
		if (conex == null) {
			conex = new Connections();
		} else {
			return conex;
		}
		return conex;

	}

}