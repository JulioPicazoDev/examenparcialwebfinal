<%@
   page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="ISO-8859-1"
%>

<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<head>
    <meta charset="ISO-8859-1">
    <title>Actividades</title>
</head>

<body>
	<h1>Selecciona la opcion que quieras: </h1>
    <ul>
		<li><a href="GoToAdd">Agregar</a></li>
        <li><a href="GetActs">Lista de TODAS las actividades</a></li>
        <li><a href="GetActs?done=0">Lista de actividades NO hechas</a></li>
        <li><a href="GetActs?done=1">Lista de actividades hechas</a></li>
    </ul>
</body>

</html>